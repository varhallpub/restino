<?php

namespace Varhall\Restino\Utils\Transformation\Transformators;

/**
 * Description of ITransformator
 *
 * @author sibrava
 */
interface ITransformator
{
    public function apply($value);
}
